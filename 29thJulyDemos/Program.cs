﻿using System.IO;
namespace _29thJulyDemos
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //StringWriter writer = new StringWriter();
            //writer.WriteLine("Hello");
            //writer.WriteLine("Buy");
            //writer.Close();

            //StringReader reader = new StringReader(writer.ToString());
            //Console.WriteLine(reader.ReadToEnd());
            //reader.Close();
            //\n\t
            if (!Directory.Exists(@"C:\CGI-Demos"))
                Directory.CreateDirectory(@"C:\CGI-Demos");
            StreamWriter writer = new StreamWriter(@"C:\CGI-Demos\file100.txt");
            writer.WriteLine("Hello1");
            writer.WriteLine("Hello2");
            writer.Close();

            StreamReader reader = new StreamReader
                (@"C:\CGI-Demos\file100.txt");
            string line = reader.ReadLine();
            while (line != null)
            {
                Console.WriteLine(line);
                line = reader.ReadLine();

            }
            reader.Close();

            Console.WriteLine("Contents char by char");
            reader = new StreamReader(@"C:\CGI-Demos\file100.txt");
        int c =reader.Read();
            while (c >= 0)
            {
                Console.WriteLine((char)c);
                c= reader.Read();
            }
            reader.Close();
            reader = new StreamReader (@"C:\CGI-Demos\file100.txt");
            Console.WriteLine(reader.ReadToEnd());
            reader.Close ();

            // FILESTREAM

            FileStream fs = null;
            StreamWriter writer1 = null;

            try
            { fs= new FileStream(@"C:\CGI-Demos\file101.txt",
                FileMode.Append, FileAccess.Write);
                 writer1 = new StreamWriter(fs);
                string ch = "y";
                string name = string.Empty;
                while (ch == "y")
                {
                    Console.WriteLine("Enter Name");
                    name = Console.ReadLine();
                    writer1.WriteLine(name);
                    Console.WriteLine("Enter More name?");
                    ch = Console.ReadLine();

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                writer1.Close();
                fs.Close();
            }

            fs = null;
            StreamReader reader1 = null;
            try
            {
                fs = new FileStream(@"C:\CGI-Demos\file101.txt", FileMode.Open, FileAccess.Read);
                reader1 = new StreamReader(fs);
                Console.WriteLine(reader1.ReadToEnd());
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                reader1.Close();
                fs.Close();
            }


        }

    }
}