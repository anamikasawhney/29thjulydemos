﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _29thJulyDemos
{
    // struct and class 
    // struct is value type varibale
    // class is reference type variable
    // struct does not allow inheritance
    // there cus be only 1 constr in stru,that too shud be fully parametrized
    // struc so not follow concepts of OOPS
    // Why structure
    // Structures are used to store logically related data
    // OTHERWISE STRUCT WILL SAVE TIME > BEC THEY ARE VAKUE TYPE VAAIBLAES
    // VALUE TYPE VARIBALE ARE STORED IN STACK
    // GARBAGE COLLECTION RUNS ON HEAP  NOT ON STACK

    struct DOB
    {
        int dd, mm,yy;
    }
    struct Address
    {
        string streetNo;
        string city;
        string state;
        string country;
        string zip;
    }
    //struct Student
    //{
    //    int id;
    //    string name;
    //    Address address;   
    //    public void Get() { }
    //    public void Dispplay() { }
    //    public Student(int id, string name , Address address)
    //    {
    //        this.id = id;
    //        this.name = name;
    //        this.address = address;

    //    }
    //}
    //struct PartTimeStudent : Student
    // {

    // }
    internal class Student
    {
        string name;
        
        DOB dob;
        Address address;

    }
}
