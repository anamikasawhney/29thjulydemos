﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _29thJulyDemos
{
    // Ayush created a class which has 5 methods
    // Abhishek is using your class
    // Abhishek will use all methods of your class, but he wants to
    // add 2 more methods to that class without affecting Ayush's original class
    // Such methods which are added to a class are ka extension methods

    internal class Ayush
    {

        public int A()
        {
            return 0;
        }
        public int B()
        {
            return 0;
        }
        public int C()
        {
            return 0;
        }
        public int D()
        {
            return 0;
        }
        public int E()
        {
            return 0;
        }


    }
    static class Extended
    {
        public static bool IsEven(this int classint, int num)
        {
            if (num % 2 == 0) return true;
            else
                return false;

        }
    }
    static class Calling
    { 
        static void Main()
        {
            // Want to add Prime Function in INt class
            int x = 10;
            List<int> list = new List<int>() { 1, 2, 3, 4 };
            Console.WriteLine(x.IsEven(10));

        }
    }
}
