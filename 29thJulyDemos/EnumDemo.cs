﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _29thJulyDemos
{ 
    enum  Choice: byte{ Add = 1, Subtract , Product , Divide};

    internal class EnumDemo
    {
        static void Main()
        {
            // enum > User Defined class
            // class > when you want to store data about some entity, and also methods

            // enum > 1. They are used to assign numeric constants to strings
            // 2. They are useful for developers
            //3 . They are used when we have some limited options / choices
            // Perform some operation depending upon choice

            int x, y, choice;
            Console.WriteLine("ENter No1");
            x = Byte.Parse(Console.ReadLine());
            Console.WriteLine("ENter No1");
            y = Byte.Parse(Console.ReadLine());
            Console.WriteLine("ENter choice");
            choice = Byte.Parse(Console.ReadLine());
           switch(choice)
            {
                //case 1: Console.WriteLine(x + y); break;
                case (int)Choice.Add: Console.WriteLine(x + y); break;

                //case 2: Console.WriteLine(x - y); break;
                case (int)Choice.Subtract: Console.WriteLine(x - y); break;

                //case 3: Console.WriteLine(x * y); break;
                case (int) Choice.Product: Console.WriteLine(x * y); break;

                //case 4: Console.WriteLine(x / y); break;
                case (int) Choice.Divide: Console.WriteLine(x * y); break;

                default: Console.WriteLine("Invalid choice"); break;
            }


        }
    }
}
